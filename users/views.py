from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django import forms
from .forms import UserRegistrationForm

def logout_view(request):
  """Log the user out."""
  logout(request)
  return HttpResponseRedirect(reverse('pizzas:pizzas'))

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            email =  userObj['email']
            password =  userObj['password']
            password2 = userObj['password_two']
            if not (User.objects.filter(username=username).exists()
              or User.objects.filter(email=email).exists()
              or password != password2):
                User.objects.create_user(username, email, password)
                user = authenticate(username = username, password = password)
                login(request, user)
                return HttpResponseRedirect(reverse('pizzas:pizzas'))
            else:
                raise forms.ValidationError('Validation failed.')
    else:
        form = UserRegistrationForm()
    return render(request, 'users/register.html', {'form' : form})

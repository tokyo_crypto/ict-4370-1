from django import forms

class UserRegistrationForm(forms.Form):
    username = forms.CharField(
        required = True,
        label = 'Username',
        max_length = 32
    )
    email = forms.CharField(
        required = True,
        label = 'Email',
        max_length = 32,
    )
    password = forms.RegexField(
        regex = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$',
        required = True,
        label = 'Password',
        max_length = 32,
        widget = forms.PasswordInput()
    )
    password_two = forms.RegexField(
        regex = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$',
        required = True,
        label = 'Password (Again)',
        max_length = 32,
        widget = forms.PasswordInput()
    )
    first_name = forms.CharField(
        required = True,
        label = 'First name',
        max_length = 32
    )
    last_name = forms.CharField(
        required = True,
        label = 'Last name',
        max_length = 32
    )
    phone_number = forms.CharField(
        required = True,
        label = 'Phone',
        max_length = 20
    )
    address = forms.CharField(
        required = True,
        label = 'Address',
        max_length = 32
    )
    address_line_two = forms.CharField(
        required = False,
        label = 'Address (Line 2)',
        max_length = 32
    )
    town = forms.CharField(
        required = True,
        label = 'Town',
        max_length = 32
    )
    zipcode = forms.CharField(
        required = True,
        label = 'Zipcode',
        max_length = 5
    )
    state = forms.CharField(
        required = True,
        label = 'State',
        max_length = 2
    )
"""Defines URL patterns for pizzas."""
from django.urls import path
from . import views

app_name="pizzas"

urlpatterns = [
  # Home page
  path('home', views.home, name='home'),
  # Pizza list.
  path('', views.pizzas, name='pizzas'),
  # Pizza order
  path('<int:pizza_id>/order/', views.order, name='order'),
  # Pizza details page
  path('<int:pizza_id>/', views.pizza, name='pizza'),
]

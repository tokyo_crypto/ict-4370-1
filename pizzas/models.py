from django.db import models

class Pizza(models.Model):
  name = models.CharField(max_length=200)
  created_at = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.name

class Topping(models.Model):
  name = models.CharField(max_length=200)
  pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
  created_at = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.name
from django.shortcuts import render
from .models import Pizza
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.

def home(request):
  """The home page for Pizza"""
  return render(request, 'pizzas/index.html')


def pizzas(request):
  """Show all pizzas."""
  pizzas = Pizza.objects.all()
  context = {'pizzas': pizzas}
  return render(request, 'pizzas/pizzas.html', context)

def pizza(request, pizza_id):
  """Show a single pizza and all its toppings."""
  pizza = Pizza.objects.get(id=pizza_id)
  toppings = pizza.topping_set.all()
  context = {'pizza': pizza, 'toppings': toppings}
  return render(request, 'pizzas/pizza.html', context)

def order(request, pizza_id):
  if request.user.is_authenticated:
    """Order a pizza"""
    pizza = Pizza.objects.get(id=pizza_id)
    toppings = pizza.topping_set.all()
    context = {'pizza': pizza, 'toppings': toppings}
    return render(request, 'pizzas/order.html', context)
  else:
    return HttpResponseRedirect(reverse('users:login'))
